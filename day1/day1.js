/**
 * Created by Nilyan Vicent on 18/12/2017.
 */

var canvas = document.getElementById('base');
/*Width of canvas*/

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');
/*
c.fillRect(100,100,100,100)

/*Line

c.beginPath();
c.moveTo(50,300);
c.lineTo(300,100);
c.lineTo(400,300);
c.strokeStyle = "#cc4447";
c.stroke();/

//arc /circle
/*Reset properties
c.beginPath();
c.arc(300,300,30,0,Math.PI*2, false);
c.strokeStyle ='salmon';
c.stroke();*/

/*arc /circle
for(var i=0; i<100; i++){
    var x = Math.random() * window.innerWidth;
    var y = Math.random() * window.innerHeight;

    /*Reset properties
    c.beginPath();
    c.arc(x,y,30,0,Math.PI*2, false);
    c.strokeStyle ='salmon';
    c.stroke();
}*/

function Circle(x,y,dx,dy,radius) {
    /*Objects*/
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;

    /*This draw the circles*/
    this.draw = function () {
        c.beginPath(); //Draw paths on the canvas
        c.arc(this.x,this.y,this.radius,0,Math.PI*2, false);
        c.strokeStyle ='salmon';
        c.stroke(); //Draw a path with its style
    }
    /*This update the position of a circle*/
    this.update = function () {
        if (this.x + this.radius >innerWidth||this.x-this.radius < 0){
           this.dx=-this.dx;
        }
        if (this.y + this.radius >innerHeight||this.y-this.radius < 0){
            this.dy=-this.dy;
        }
        this.x+=this.dx;
        this.y+=this.dy;
        this.draw();
    }
}
var circleArray =[];

for(var i =0; i<100; i++){
    var x = Math.random() *innerWidth;
    var y = Math.random() *innerHeight;
    var dx = (Math.random() - 0.5)*2;
    var dy = (Math.random() - 0.5)*2;
    var radius =30;
    circleArray.push(new Circle(x,y,dx,dy,radius));
}


 function animate() {
     requestAnimationFrame(animate);
     c.clearRect(0,0, innerWidth, innerHeight);

     for(var i =0; i<circleArray.length; i++){
         circleArray[i].update();
     }
 }

this.animate();
